def calculateResultingVector(decisionMatr: list[list[int]], stateProbabilityVec: list[float]) -> list[float]:
    resVector: list[float] = []
    for i in range(len(decisionMatr)):
        rowSum: float = 0
        for j in range(len(decisionMatr[i])):
            rowSum += decisionMatr[i][j] * stateProbabilityVec[j]
        resVector.append(rowSum)
    return resVector


def determineTheBestOutcome(resVec: list[float]) -> None:
    maxGainIndex: int = 0
    for i in range(1, len(resVec)):
        if resVec[i] > resVec[maxGainIndex]:
            maxGainIndex = i
    print()
    print("=" * 70)
    print("RESULT: The best outcome is a" + str(maxGainIndex + 1) + ". Probable gain is " + str(resVec[maxGainIndex]))
    print("=" * 70)


def printDecisionMatrix(decMatr: list[[list[int]]]) -> None:
    print("\nDecision Matrix:")
    for i in range(len(decMatr)):
        print(decMatr[i])
    print()


if __name__ == '__main__':
    stateProbabilityVector: list[float] = [0.4, 0.25, 0.2, 0.15]
    decisionMatrix: list[list[int]] = [
        [5, 10, 18, 25],
        [8, 7, 8, 23],
        [21, 18, 12, 21],
        [30, 22, 19, 15]
    ]
    print("State probability vector: " + str(stateProbabilityVector))
    printDecisionMatrix(decisionMatrix)
    resultingVector: list[float] = calculateResultingVector(decisionMatrix, stateProbabilityVector)
    print("Probable gain vector: " + str(resultingVector))
    determineTheBestOutcome(resultingVector)
